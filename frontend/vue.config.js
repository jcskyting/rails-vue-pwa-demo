module.exports = {
    // proxy API requests to Rails during development

    // output built static files to Rails's public dir.
    // note the "build" script in package.json needs to be modified as well.
    outputDir: "../public",

    // modify the location of the generated HTML file.
    // make sure to do this only in production.
    indexPath: "../app/views/application.html.erb",
    publicPath: '/' ,

    chainWebpack: config => {
        config.devServer
        .hotOnly(true)
        .writeToDisk(filePath => true);
    },
};