# README

## run as development
1. rails s
2. cd frontend && yarn serve

## run as production
1. cd frontend && yarn build
2. rails s

## clean
1. cd frontend && yarn clean